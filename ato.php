<?php
	
    /* 	    

        Create a PHP class to determine the tax due for an Ausralian employee.
        
        1. It should match the results of this calculator
        
        https://www.ato.gov.au/Calculators-and-tools/Host/?anchor=TWC&anchor=TWC#TWC/questions
        
        https://www.ato.gov.au/Rates/Tax-tables/


	    Usage: 

	    $tc = new TaxCalculation_Australia();
	    $options = array(); 
	    
	    $options[TaxCalculation_Australia::OPTION_TAX_FILE_NUMBER_PROVIDED] = true;
	    $options[TaxCalculation_Australia::OPTION_TAX_STATUS] = "resident";
	    
	    echo $tc->calculateTax(67121, $options)['tax_due'];

    */

    class TaxCalculation_Australia {
	    
		const TAX_PERIOD_WEEKLY = 1; 
		const TAX_PERIOD_FORTNIGHTLY = 2; 		
		const TAX_PERIOD_MONTHLY = 3; 	
		
		const OPTION_TAX_FILE_NUMBER_PROVIDED = 0;
		const OPTION_TAX_STATUS = 1;
		const OPTION_CLAIMING_TAX_FREE_THRESHOLD = 2;		
		const OPTION_FEEHELP_SSSL_ORTSL_DEBT = 3;
		const OPTION_SFSS_DEBT = 4;
		const OPTION_TAX_OFFSETS = 5;
		const OPTION_MEDICARE_LEVY_EXEMPTION = 6;
		const OPTION_MEDICARE_LEVY_REDUCTION = 7;
		const OPTION_HAS_SPOUSE = 8;		
		const OPTION_NUM_CHILDREN_CLAIMED = 9;		

		public function __construct() { }

		public function calculateTax($taxable_earnings, $options = array()) {

			$result = array(); 			

			$result['taxable_earnings'] = $taxable_earnings;
			$result['tax_due'] = 32;			

			return $result; 
		}
		
    
    
    }
	
?>